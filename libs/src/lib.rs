
pub use tracing::{span, Level, event, trace, debug, info, warn, error};
pub use dotenv;
pub use anyhow;
pub use thiserror;
pub use text_io;
pub use enum_display_derive;

use tracing_subscriber::EnvFilter;
#[allow(unused_imports)]
use tracing_subscriber::layer::SubscriberExt;


pub fn init_logging(max_level: Option<Level>) -> anyhow::Result<()> {
    let mut env_filter = EnvFilter::try_from_default_env()
        .unwrap_or_else(|_| EnvFilter::new("trace"));

    if let Some(max_level) = max_level {
        env_filter = env_filter.add_directive(max_level.into());
    }

    #[cfg(feature = "logging_plain")]
        let subscriber = tracing_subscriber::fmt()
        .with_env_filter(env_filter)
        .with_max_level(max_level)
        .finish();

    #[cfg(feature = "logging_json")]
        let formatting_layer = tracing_bunyan_formatter::BunyanFormattingLayer::new(
        "zero2prod".into(),
        //output formatted spans to stdout
        std::io::stdout,
    );
    #[cfg(feature = "logging_json")]
        let subscriber = tracing_subscriber::Registry::default()
        .with(env_filter)
        .with(tracing_bunyan_formatter::JsonStorageLayer)
        .with(formatting_layer);


    #[cfg(feature = "logging_pretty")]
        let subscriber = tracing_subscriber::fmt()
        .with_max_level(max_level)
        .with_env_filter(env_filter)
        .pretty()
        //build but do not install subscriber
        .finish();

    //install subscriber as global default
    tracing::subscriber::set_global_default(subscriber)?;

    // event is a span that cannot be entered and represents some specific thing
    event!(Level::TRACE, "Tracing Events Initialized");

    // Construct a new span named "my span".
    let span = span!(Level::TRACE, "Logging Initializing Spans");

    // Dropping the span will close it, indicating that it has ended.
    span.in_scope(|| {
        // Any trace events in this closure or code called by it will occur within
        // the span.

        trace!("Tracing spans warmed up");
    });

    Ok(())
}





#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        let result = 2 + 2;
        assert_eq!(result, 4);
    }
}
