#![allow(dead_code)]

use std::fmt::Display;
use std::string::ToString;
use libs::enum_display_derive::Display;

#[derive(Clone, Debug)]
pub struct Token {
	pub token_kind: TokenKind,
	pub line: u16,
}

impl Token {
	pub fn new(token_kind: TokenKind,line: u16) -> Self {
		Token {
			token_kind,
			line,
		}
	}
}

#[derive(Clone, Debug, Display, PartialEq)]
pub enum TokenKind {
	Illegal(char),
	EOF,

	// Literals are stored as strings
	Ident(String),
	Integer(i64),
	String(String),

	// Operators
	Assign,
	Plus,
	Minus,
	Bang,
	Asterisk,
	Slash,
	LessThan,
	GreaterThan,
	Equal,
	NotEqual,

	// Delimiters
	Comma,
	Semicolon,
	LeftParen,
	RightParen,
	LeftBrace,
	RightBrace,
	Dot,

	// Keywords
	Function,
	Var,
	True,
	False,
	If,
	Else,
	Return,
	And,
	Class,
	For,
	Nil,
	Or,
	Print,
	Super,
	This,
	While,
}

impl Default for TokenKind {
	// Choose an Illegal identifier as default
	// this should be overriden before being used
	fn default() -> TokenKind {
		return TokenKind::Illegal('\0');
	}
}

pub fn lookup_keyword(ident: &str) -> TokenKind {
	match ident {
		"." => TokenKind::Dot,
		"fn" => TokenKind::Function,
		"var" => TokenKind::Var,
		"true" => TokenKind::True,
		"false" => TokenKind::False,
		"if" => TokenKind::If,
		"else" => TokenKind::Else,
		"return" => TokenKind::Return,
		"and" => TokenKind::And,
		"class" => TokenKind::Class,
		"for" => TokenKind::For,
		"nil" => TokenKind::Nil,
		"or" => TokenKind::Or,
		"print" => TokenKind::Print,
		"super" => TokenKind::Super,
		"this" => TokenKind::This,
		"while" => TokenKind::While,
		_ => TokenKind::Ident(ident.to_string()),
	}
}

#[test]
fn lookup_ident_test() {
	assert_eq!(lookup_keyword("fn"), TokenKind::Function);
}

#[test]
fn test_default_token() { assert_eq!(TokenKind::default(), TokenKind::Illegal('\0')); }
