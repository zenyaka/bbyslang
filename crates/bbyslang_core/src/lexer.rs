use crate::token::{Token, TokenKind};
use std::iter::{Peekable};
use std::str::Chars;
use std::thread::current;
use libs::{error, info};

#[allow(unused_imports)]
fn is_letter(c: char) -> bool {
	c.is_alphabetic() || c == '_'
}

fn is_linebreak(input_char: char) -> bool {
	match input_char {
		'\x0A'  => {
			//info!("0A");
			true
		},
		'\x0B'  => {
			//info!("0B");
			true
		},
		'\x0C'  => {
			//info!("0C");
			true
		},
		//'\x0D' => {
		//	info!("0D");
		//	true
		//},
		//'\x0C' | '\x0D' => true,
		_ => false,
	}
}

pub struct Lexer<'a> {
	pub input_iter: Peekable<Chars<'a>>,
	pub current_line: u16,
}


impl<'a> Lexer<'a> {
	pub fn new(input: &'a str) -> Lexer<'a> {
		Lexer {
			input_iter: input.chars().peekable(),
			current_line: 0,
		}
	}

	fn peek_next(&mut self) -> Option<&char> {
		self.input_iter.peek()
	}

	fn take_next(&mut self) -> Option<char> {
		self.input_iter.next()
	}

	fn take_next_stripped(&mut self) -> Option<char> {
		let res = self.input_iter.next();
		self.strip_whitespace();
		res
	}

	fn get_ident_token_string(&mut self, first_char: char) -> String {
		let mut token = String::new();
		token.push(first_char);

		loop {
			let next = self.peek_next();
			match next {
				Some(c) => {
					if is_letter(*c) {
						token.push(self.take_next().expect("Next somehow didn't exist"));
					} else {
						break; // we found something that isn't a letter, so we're done
					}
				}
				None => { break; } //out of letters, we done
			}
		}

		token
	}

	fn get_integer(&mut self, first_char: char) -> i64 {
		let mut token = String::new();
		token.push(first_char);

		loop {
			let next = self.peek_next();
			match next {
				Some(c) => {
					if c.is_numeric() {
						token.push(self.take_next().expect("Next somehow didn't exist"));
					} else {
						break; // we found something that isn't a letter, so we're done
					}
				}
				None => { break; } //out of letters, we done
			}
		}

		token.parse::<i64>().expect("Integer wasn't parseable as i64")
	}


	fn strip_whitespace(&mut self) {
		//strip whitespace NOT newlines
		loop {
			match self.peek_next() {
				Some(&c) => {
					if is_linebreak(c) {
						//info!("Found line break");
						break;
					} else if c.is_whitespace() {
						let _ = self.take_next();
					} else if c == '\x09' {
						error!("CURSED");
						break;
					}
					else {
						break;
					}
				}
				None => break,
			}
		}
	}

	//fn is_linebreak(input_char: char) -> bool {
	//	match input_char {
	//		'\x0A'  => {
	//			//info!("0A");
	//			true
	//		},
	//		'\x0B'  => {
	//			//info!("0B");
	//			true
	//		},
	//		'\x0C'  => {
	//			//info!("0C");
	//			true
	//		},
	//		//'\x0D' => {
	//		//	info!("0D");
	//		//	true
	//		//},
	//		//'\x0C' | '\x0D' => true,
	//		_ => false,
	//	}
	//}


	fn handle_keywords(word: &str) -> TokenKind {
		match word {
			"var" => TokenKind::Var,
			"if" => TokenKind::If,
			"else" => TokenKind::Else,
			"true" => TokenKind::True,
			"false" => TokenKind::False,
			"fn" => TokenKind::Function,
			"return" => TokenKind::Return,
			_ => {
				TokenKind::Ident(word.to_owned())
			}
		}
	}

	//todo: Remove this once done with lexer
	pub fn get_all_tokens(&mut self) -> Vec<Token> {
		let mut tokens: Vec<Token> = Vec::new();

		let mut current_token = self.next_token();
		let mut has_tokens = true;

		while has_tokens {
			match current_token.token_kind {
				TokenKind::EOF => {
					info!("Token EOF Found");
					has_tokens = false;
					tokens.push(current_token.clone());
				}

				_ => {
					info!("Token is {} on line {}", &current_token.token_kind, &current_token.line);

					tokens.push(current_token.clone());
					current_token = self.next_token();
				}
			}
		}

		//while current_token != Token::EOF {
		//	tokens.push(current_token);
		//}

		tokens
	}

	pub fn next_token(&mut self) -> Token {
		self.strip_whitespace();

		let next = self.take_next();

		let token_kind = self.get_token_kind_from_char(next);

		Token {
			token_kind,
			line: self.current_line,
		}
	}

	fn get_token_kind_from_char(&mut self, mut next_char: Option<char>) -> TokenKind {
		if next_char.is_none() {
			return TokenKind::EOF;
		}
		let test_char = next_char.unwrap();
		let mut curr_char= test_char;
		if !is_linebreak(test_char) && test_char.is_whitespace() {
			//in the interest of being as un-maintanble as possible, skip the second of \r\n endlines
			curr_char = self.take_next_stripped().unwrap();

		}

		return match curr_char {
			';' => { TokenKind::Semicolon }
			'(' => { TokenKind::LeftParen }
			')' => { TokenKind::RightParen }
			',' => { TokenKind::Comma }
			'+' => { TokenKind::Plus }
			'{' => { TokenKind::LeftBrace }
			'}' => { TokenKind::RightBrace }
			'-' => { TokenKind::Minus }
			'/' => { TokenKind::Slash }
			'*' => { TokenKind::Asterisk }
			'<' => { TokenKind::LessThan }
			'>' => { TokenKind::GreaterThan }
			'=' => {
				match self.peek_next() {
					Some(&c) if c == '=' => {
						let _ = self.take_next();
						TokenKind::Equal
					}
					_ => TokenKind::Assign
				}
			}
			'!' => {
				match self.peek_next() {
					Some(&c) if c == '=' => {
						let _ = self.take_next();
						TokenKind::NotEqual
					}
					_ => TokenKind::Bang
				}
			}
			_ => {
				if is_letter(curr_char) {
					let token = self.get_ident_token_string(curr_char);
					let tk = Lexer::handle_keywords(&token);
					return tk;
				} else if curr_char.is_numeric() {
					TokenKind::Integer(self.get_integer(curr_char))
				} else if is_linebreak(curr_char) {

					let maybe_next_option = self.peek_next();
					if maybe_next_option.is_none() {
						return TokenKind::EOF;
					}
					let maybe_next =maybe_next_option.unwrap();
					if is_linebreak(maybe_next.clone()) {
						let _crap = self.take_next_stripped();
					}
					self.current_line += 1;

					let next = self.take_next_stripped();


					self.get_token_kind_from_char(next)
				} else {
					panic!("Found illegal char: {}", curr_char);

					#[allow(unreachable_code)]
						TokenKind::Illegal(curr_char)
				}
			}
		};
	}
}


#[test]
fn test_next_token_with_symbols() {
	let input = "=+(){},;";
	let tests: [TokenKind; 9] = [
		{ TokenKind::Assign },
		{ TokenKind::Plus },
		{ TokenKind::LeftParen },
		{ TokenKind::RightParen },
		{ TokenKind::LeftBrace },
		{ TokenKind::RightBrace },
		{ TokenKind::Comma },
		{ TokenKind::Semicolon },
		{ TokenKind::EOF },
	];

	let mut lexer = Lexer::new(input);
	for test in tests.iter() {
		assert_eq!(*test, lexer.next_token());
	}
}

#[test]
fn test_next_token_extended() {
	let input = r#"let five = 5;
		let ten = 10;
		let add = fn(x, y) {
		x + y;
		};
		let result = add(five, ten);"#;

	let tests = [
		TokenKind::Var,
		TokenKind::Ident("five".to_owned()),
		TokenKind::Assign,
		TokenKind::Integer(5),
		TokenKind::Semicolon,
		TokenKind::Var,
		TokenKind::Ident("ten".into()),
		TokenKind::Assign,
		TokenKind::Integer(10),
		TokenKind::Semicolon,
		TokenKind::Var,
		TokenKind::Ident("add".into()),
		TokenKind::Assign,
		TokenKind::Function,
		TokenKind::LeftParen,
		TokenKind::Ident("x".into()),
		TokenKind::Comma,
		TokenKind::Ident("y".into()),
		TokenKind::RightParen,
		TokenKind::LeftBrace,
		TokenKind::Ident("x".into()),
		TokenKind::Plus,
		TokenKind::Ident("y".into()),
		TokenKind::Semicolon,
		TokenKind::RightBrace,
		TokenKind::Semicolon,
		TokenKind::Var,
		TokenKind::Ident("result".into()),
		TokenKind::Assign,
		TokenKind::Ident("add".into()),
		TokenKind::LeftParen,
		TokenKind::Ident("five".into()),
		TokenKind::Comma,
		TokenKind::Ident("ten".into()),
		TokenKind::RightParen,
		TokenKind::Semicolon,
		TokenKind::EOF
	];

	let mut lexer = Lexer::new(input);
	for (size, test) in tests.iter().enumerate() {
		info!("Found sized: {} token {:?}", size, test);
		assert_eq!(*test, lexer.next_token());
	}
}

#[test]
fn test_full_tokens() {
	let input = r#"  let five = 5;
                            let ten = 10;

                            let add = fn(x, y) {
                              x + y;
                            };

                            let result = add(five, ten);
                            !-/*5;
                            5 < 10 > 5;

                            if (5 < 10) {
                                return true;
                            } else {
                                return false;
                            }

                            10 == 10;
                            10 != 9;"#;

	let tests = [
		TokenKind::Var,
		TokenKind::Ident("five".to_owned()),
		TokenKind::Assign,
		TokenKind::Integer(5),
		TokenKind::Semicolon,
		TokenKind::Var,
		TokenKind::Ident("ten".to_owned()),
		TokenKind::Assign,
		TokenKind::Integer(10),
		TokenKind::Semicolon,
		TokenKind::Var,
		TokenKind::Ident("add".to_owned()),
		TokenKind::Assign,
		TokenKind::Function,
		TokenKind::LeftParen,
		TokenKind::Ident("x".to_owned()),
		TokenKind::Comma,
		TokenKind::Ident("y".to_owned()),
		TokenKind::RightParen,
		TokenKind::LeftBrace,
		TokenKind::Ident("x".to_owned()),
		TokenKind::Plus,
		TokenKind::Ident("y".to_owned()),
		TokenKind::Semicolon,
		TokenKind::RightBrace,
		TokenKind::Semicolon,
		TokenKind::Var,
		TokenKind::Ident("result".to_owned()),
		TokenKind::Assign,
		TokenKind::Ident("add".to_owned()),
		TokenKind::LeftParen,
		TokenKind::Ident("five".to_owned()),
		TokenKind::Comma,
		TokenKind::Ident("ten".to_owned()),
		TokenKind::RightParen,
		TokenKind::Semicolon,
		TokenKind::Bang,
		TokenKind::Minus,
		TokenKind::Slash,
		TokenKind::Asterisk,
		TokenKind::Integer(5),
		TokenKind::Semicolon,
		TokenKind::Integer(5),
		TokenKind::LessThan,
		TokenKind::Integer(10),
		TokenKind::GreaterThan,
		TokenKind::Integer(5),
		TokenKind::Semicolon,
		TokenKind::If,
		TokenKind::LeftParen,
		TokenKind::Integer(5),
		TokenKind::LessThan,
		TokenKind::Integer(10),
		TokenKind::RightParen,
		TokenKind::LeftBrace,
		TokenKind::Return,
		TokenKind::True,
		TokenKind::Semicolon,
		TokenKind::RightBrace,
		TokenKind::Else,
		TokenKind::LeftBrace,
		TokenKind::Return,
		TokenKind::False,
		TokenKind::Semicolon,
		TokenKind::RightBrace,
		TokenKind::Integer(10),
		TokenKind::Equal,
		TokenKind::Integer(10),
		TokenKind::Semicolon,
		TokenKind::Integer(10),
		TokenKind::NotEqual,
		TokenKind::Integer(9),
		TokenKind::Semicolon,
		TokenKind::EOF,
	];

	let mut lexer = Lexer::new(input);
	for (size, test) in tests.iter().enumerate() {
		println!("Found sized: {} token {:?}", size, test);
		assert_eq!(*test, lexer.next_token());
	}
}
