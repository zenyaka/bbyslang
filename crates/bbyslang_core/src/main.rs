#![feature(io_read_to_string)]

mod token;
mod lexer;

use std::fs::File;
use std::io::Read;
use libs::{anyhow::Result, info, error, Level};
use libs::text_io::read;
use crate::lexer::Lexer;

fn run(input_string: &str) -> Result<()> {
	let mut lexer = Lexer::new(input_string);
	let tokens = lexer.get_all_tokens();

	Ok(())
}

fn main() -> Result<()> {
	let args: Vec<String> = std::env::args().collect();
	if args.len() > 2 {
		println!("Usage: zen [script]");
	} else {
		libs::init_logging(Some(Level::INFO))?;

		if args.len() == 2 {
			let file_path = args[1].clone();
			run_file(file_path)?;
		} else {
			run_prompt()?;
		}
	}
	Ok(())
}


fn run_prompt() -> Result<()> {
	let mut line: String = String::new();

	while !line.eq("exit;") {
		line = read!("{}\n");
		//println!("Read: {}", &line);
		run(&line)?;
	}
	info!("Exited repl");

	Ok(())
}

fn run_file(file_path: String) -> Result<()> {
	let mut file_contents: String = String::new();

	match File::open(file_path) {
		Ok(mut file) => {
			file.read_to_string(&mut file_contents)?;

			//println!("{}", file_contents);
		}
		Err(error) => {
			error!("Error opening file {}", error);
		}
	}

	run(&file_contents)
}
